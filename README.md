# dotfiles

My configuration files

## Usage

To use the configuration on a brand new system, run the following commands:

```
stow --adopt --verbose=2 --dir=. --target=$HOME .
git restore .
```
