" ~/.vimrc

" basics
filetype plugin on   " enable filetype auto-detection
syntax on            " enable syntax highlighting
try
  colorscheme sorbet " odd choice but it looks nice
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme slate  " can use this if sorbet unavailable
endtry

set noexpandtab      " love me tabs, simple as
set tabstop=4        " divinely commanded tab width
set encoding=utf-8   " ensure encoding is UTF-8
set number           " show absolute line numbers
set modeline

if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
let &cpo=s:cpo_save
unlet s:cpo_save

set backspace=indent,eol,start
set display=truncate
set fileencodings=ucs-bom,utf-8,default,latin1
set helplang=en
set history=200
set incsearch
set langnoremap
set nolangremap
set nrformats=bin,hex
set ruler
set scrolloff=10
set showcmd
set ttimeout
set ttimeoutlen=100
set wildmenu

" Search selected text using //
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>

" Put plugins and dictionaries in this dir (also on Windows)
let vimDir = '$HOME/.vim'
let &runtimepath.=','.vimDir

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
	let myUndoDir = expand(vimDir . '/undodir')
	" Create dirs
	call system('mkdir ' . vimDir)
	call system('mkdir ' . myUndoDir)
	let &undodir = myUndoDir
	set undofile
endif

" Automatic vim-plug installation (requires curl)
"let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
let data_dir = '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" vim-plug begin
call plug#begin()

Plug 'sheerun/vim-polyglot'
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'earthly/earthly.vim', { 'branch': 'main' }

" vim-plug end
call plug#end()

" disable airline powerline fonts
let g:airline_powerline_fonts = 0
let g:airline_left_sep=''
let g:airline_right_sep=''

" use normal symbols in line and column section
" https://github.com/vim-airline/vim-airline/discussions/2585
" https://vi.stackexchange.com/questions/22046/how-to-remove-line-column-from-airline-section-z
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:airline_symbols.linenr = ''
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.colnr = ':'
au User AirlineAfterInit :let g:airline_section_z = airline#section#create(['%3p%% ','linenr','colnr'])

" set airline theme
let g:airline_theme = 'angr'
let g:airline_theme = 'onedark'

" vim: set ft=vim :
