#!/bin/sh

URL="${1}"
TOKEN="${2}"
TAGS="${3}"
NOTE="${4}"

echo '$1 is URL'
echo '$2 is TOKEN'
echo '$3 is TAGS'
echo '$4 is NOTE'
echo 'edit the script to remove the "exit" command so the script will run'
exit

docker run --rm --name gitlab-runner -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
	--non-interactive \
	--executor "docker" \
	--docker-image alpine:latest \
	--url "${URL}" \
	--registration-token "${TOKEN}" \
	--description "runner1" \
	--maintenance-note "${NOTE}" \
	--tag-list "${TAGS}" \
	--run-untagged="false" \
	--locked="false" \
	--access-level="not_protected"
