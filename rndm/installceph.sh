#!/bin/sh

#install prerequisites
apt install -y gnupg uuid-runtime
#add key
wget -q -O- 'https://download.ceph.com/keys/release.asc' | apt-key add -
#add repo
echo deb https://download.ceph.com/debian-pacific/ $(lsb_release -sc) main | tee /etc/apt/sources.list.d/ceph.list
#refresh repo
apt update
#install packages
apt install -y ceph ceph-mds
#set env vars
FSID="$(uuidgen)"
HOST="$(hostname)"
IP="$(ip r | grep src | awk '{ print $9 }')"
#create directories
sudo -u ceph mkdir -p /var/lib/ceph/mon/ceph-"$HOST"
sudo -u ceph mkdir -p /var/lib/ceph/mgr/ceph-"$HOST"
sudo -u ceph mkdir -p /var/lib/ceph/mds/ceph-"$HOST"
#ceph config file
#TODO: check settings needed for a single-node thing
echo "[global]" >> /etc/ceph/ceph.conf
echo "fsid = $FSID" >> /etc/ceph/ceph.conf
echo "mon initial members = $HOST" >> /etc/ceph/ceph.conf
echo "mon host = $IP" >> /etc/ceph/ceph.conf
echo "auth cluster required = cephx" >> /etc/ceph/ceph.conf
echo "auth service required = cephx" >> /etc/ceph/ceph.conf
echo "auth client required = cephx" >> /etc/ceph/ceph.conf
echo "osd journal size = 1024" >> /etc/ceph/ceph.conf
echo "osd pool default size = 2" >> /etc/ceph/ceph.conf
echo "osd pool default min size = 1" >> /etc/ceph/ceph.conf
echo "osd pool default pg num = 333" >> /etc/ceph/ceph.conf
echo "osd pool default pgp num = 333" >> /etc/ceph/ceph.conf
echo "osd crush chooseleaf type = 1" >> /etc/ceph/ceph.conf
#add monitor config stuff
echo -n >> /etc/ceph/ceph.conf
echo "[mds.$HOST]" >> /etc/ceph/ceph.conf
echo "host = $HOST" >> /etc/ceph/ceph.conf
#keyring stuff
ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'
ceph-authtool --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring --gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd' --cap mgr 'allow r'
ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
ceph-authtool /tmp/ceph.mon.keyring --import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
chown ceph:ceph /tmp/ceph.mon.keyring
monmaptool --create --add "$HOST" "$IP" --fsid "$FSID" /tmp/monmap
sudo -u ceph ceph-mon --mkfs -i "$HOST" --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring
systemctl start ceph-mon@"$HOST"
ceph auth get-or-create mgr."$HOST" mon 'allow profile mgr' osd 'allow *' mds 'allow *' > /var/lib/ceph/mgr/ceph-"$HOST"/keyring
chown ceph:ceph /var/lib/ceph/mgr/ceph-"$HOST"/keyring
systemctl start ceph-mgr@"$HOST"
#TODO: make partition tables and format xfs
ceph-volume lvm create --data /dev/sdb1
ceph-volume lvm create --data /dev/sdc1
#fix OSD FSIDs from ceph-volume lvm list
OSD_FSID="$(ceph-volume lvm list | grep osd\.0 -A 11 | tail -n 1 | awk '{ print $3 }')"
ceph-volume lvm activate 0 "$OSD_FSID"
OSD_FSID="$(ceph-volume lvm list | grep osd\.1 -A 11 | tail -n 1 | awk '{ print $3 }')"
ceph-volume lvm activate 1 "$OSD_FSID"
ceph-authtool --create-keyring /var/lib/ceph/mds/ceph-"$HOST"/keyring --gen-key -n mds."$HOST"
ceph auth add mds."$HOST" osd "allow rwx" mds "allow *" mon "allow profile mds" -i /var/lib/ceph/mds/ceph-"$HOST"/keyring
systemctl start ceph
ceph mon enable-msgr2
ceph config set mon auth_allow_insecure_global_id_reclaim false
systemctl restart ceph
