#!/bin/sh

git config --local user.name "" # TODO: Set name
git config --local user.email "" # TODO: Set email
git config --local pull.ff only
git config --local pull.rebase preserve
git config --local push.default simple
git config --local user.signingkey 0xDEADBEEFCOFFEE69 # TODO: Set GPG key
git config --local commit.gpgsign 1
git config --local push.gpgsign 0
