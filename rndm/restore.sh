#!/bin/bash -x

emerge -av xorg-server i3-gaps i3status feh dmenu j4-dmenu-desktop pavucontrol mpd ncmpcpp alacritty dev-vcs/git dunst vim tmux lxappearance rsync eix gentoolkit xdotool xinput setxkbmap xdotool xsel
mkdir -p /home/angle/gitty/gitea-angle/dotfiles
git clone http://dockercent.hell:3000/angle/dotfiles /home/angle/gitty/gitea-angle/dotfiles/
mkdir -p /home/angle/.config/i3
mkdir -p /home/angle/.config/i3status
mkdir -p /home/angle/.config/alacritty
mkdir -p /home/angle/.config/mpd
mkdir -p /home/angle/.config/ncmpcpp
mkdir -p /home/angle/rndm
cp /home/angle/gitty/gitea-angle/dotfiles/.config/i3/config /home/angle/.config/i3/
cp /home/angle/gitty/gitea-angle/dotfiles/.config/i3status/config /home/angle/.config/i3status/
cp /home/angle/gitty/gitea-angle/dotfiles/.config/alacritty/alacritty.yml /home/angle/.config/alacritty/
cp /home/angle/gitty/gitea-angle/dotfiles/.config/mpd/mpd.conf /home/angle/.config/mpd/
cp /home/angle/gitty/gitea-angle/dotfiles/.config/ncmpcpp/config /home/angle/.config/ncmpcpp/
mv /home/angle/.bashrc /home/angle/.bashrc.default
cp /home/angle/gitty/gitea-angle/.bashrc /home/angle/
echo 'exec i3' > /home/angle/.xinitrc
