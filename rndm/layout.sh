#!/bin/bash -x
#setxkbmap -layout us -option altwin:swap_lalt_lwin
#setxkbmap -layout us -option caps:swapescape
setxkbmap -layout us -option caps:escape
setxkbmap -layout us,gr -option grp:win_space_toggle
#setxkbmap -layout us,gr -option grp:alt_shift_toggle
