# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# source aliases if they exist
test -s ~/.alias && . ~/.alias || true

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# source bash completion
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
elif [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
fi

# source git prompt script
if [ -f /usr/share/git/git-prompt.sh ]; then
    . /usr/share/git/git-prompt.sh
elif [ -f  ~/.local/share/git/git-prompt.sh ]; then
    . ~/.local/share/git/git-prompt.sh
fi

# Append .local/bin to PATH
export PATH="$PATH:/home/angle/.local/bin"

# history settings
export HISTCONTROL=ignoredups:erasedups
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="%F %T "
shopt -s histappend

# split spaces to newlines
transpose() { for arg in "$@"; do echo "$arg"; done }

# fmod: replacement for the good old modprobe -l
fmod() {
    [ "$1" = '' ] && find /lib/modules/$(uname -r) -iname *.ko -print | awk -F $(uname -r)/ '{print $NF}'
    [ "$1" != '' ] && find /lib/modules/$(uname -r) -iname "$1".ko -print | awk -F $(uname -r)/ '{print $NF}'
}

# nc replacement, disable when annoying
#nc() { $(which nc) "$(echo $1 | sed -e 's_^https*://__' -e 's_/$__')" ${*:2} ; }

# get a specific line from a pipe
line() { sed -n "${1}p"; }

# implode($glue, $pieces)
join() { local IFS="$1"; shift; echo "$*"; }

# bash auto-complete for flux
if type flux &> /dev/null; then
    . <(flux completion bash)
fi

# bash auto-complete for kubectl
if type kubectl &> /dev/null; then
    . <(kubectl completion bash)
fi

# bash auto-complete for helm
if type helm &> /dev/null; then
    . <(helm completion bash)
fi

# bash auto-complete for glow
if type glow &> /dev/null; then
    . <(glow completion bash)
fi

# bash auto-complete for pipx
if type pipx &> /dev/null; then
    eval "$(register-python-argcomplete pipx)"
fi

# bland PS1
#PS1='{\u@\h in \W} % '

# simple PS1
#PS1='\[\033[01;36m\]$ \[\033[00m\] '

# dank PS1 with dollar sign
#PS1='\[\033[01;34m\]\u\[\033[01;33m\]@\[\033[00m\]\[\033[01;35m\]\h\[\033[00m\]\[\033[01;33m\] \w \[\033[00m\]\[\033[01;37m\]\$\[\033[00m\] '

# dank PS1 with triple arrow and git prompt (need .git-prompt.sh)
#PS1='\[\033[01;34m\]\u\[\033[01;33m\]@\[\033[00m\]\[\033[01;35m\]\h\[\033[00m\]\[\033[01;33m\] \w \[\033[00m\]\[\033[01;37m\]$(__git_ps1 "(%s) ")\[\033[00m\]\[\033[01;36m\]>>>\[\033[00m\] '

# dank PS1 with triple arrow and newline and git prompt (need .git-prompt.sh)
PS1='\[\033[01;34m\]\u\[\033[01;33m\]@\[\033[00m\]\[\033[01;35m\]\h\[\033[00m\]\[\033[01;33m\] \w \[\033[00m\]\[\033[01;37m\]$(__git_ps1 "(%s) ")\[\033[00m\]\n\[\033[01;36m\]>>>\[\033[00m\] '

# so vim is used by default
export EDITOR="vim"

# turn off ansible cows
export ANSIBLE_NOCOWS=1

# make sure that the cli pinentry works correctly
export GPG_TTY=$(tty)

# so virsh doesn't need sudo
export LIBVIRT_DEFAULT_URI="qemu:///system"

# so kubectl can run without sudo in k3s
export KUBECONFIG="$HOME/.config/kube/config"

# shut up spy lang
export DOTNET_CLI_TELEMETRY_OPTOUT="true"

# get juicy terminal colors
if [[ $TERM == *256color ]]; then
    export COLORTERM="truecolor"
fi

# Might need to be in .profile or .bash_profile

# Fix Java GUI apps on dwm
# export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on"
# export _JAVA_AWT_WM_NONREPARENTING=1

# Use qt5ct theme when using a standalone window manager
# export QT_QPA_PLATFORMTHEME=qt5ct

# Load custom tty keymap
# sudo loadkeys ~/.ttymaps.kmap

# load system-specific extras
if [ -d "$HOME/.bash_extras" ]; then
    if [ ! -z "$(ls -A $HOME/.bash_extras)" ]; then
        for f in $(ls $HOME/.bash_extras/*); do source $f; done
    fi
fi
