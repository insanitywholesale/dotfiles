#!/bin/bash
setxkbmap -layout us -option altwin:swap_lalt_lwin
setxkbmap -layout us -option caps:escape
setxkbmap -layout us,gr
setxkbmap -option grp:alt_shift_toggle
