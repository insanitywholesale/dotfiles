#!/usr/bin/perl -w
use strict;
use warnings;
use Desktop::Notify;
use UI::Dialog;

my $URL = shift;
my $Vid_pattern = '^https?://(.*\.|)youtu(be\.com/watch\?v=|\.be/).*';
my $Twitch_pattern = '^https?://(.*\.|)twitch\.tv\/.*';
my $img_pattern = '.*\.(png|gif|jpeg|jpg)';
my $archive = '.*\.(zip|tar|gz)';
my $notify = Desktop::Notify->new();


sub softdie {
	system('notify-send urlhandler.pl -u critical -t 0 "$_"');
	exit;
}


if ($URL =~ $Vid_pattern) {
	my $note = $notify->create(summary=>"youtube-dl", body=>"Downloading ".$URL,timeout=>0);
	$note->show;
	open(my $status, "-|", "cd ~/Downloads/.Video; youtube-dl $URL --newline --restrict-filename --exec 'mpv --fs'") or softdie("Problem with youtube-dl: $! $?");
	while (<$status>) {
		next unless /download/;
		$note->summary("youtube-dl - ".$URL);
		$note->body($_);
		$note->show;
	}
	close $status or softdie("Couldn't stop youtube-dl process");
	$note->close;
	my $question = new UI::Dialog ( order => ['zenity','xdialog']);
	open( FILE ,"-|","youtube-dl ".$URL." --restrict-filename --get-filename");
	my $filename = <FILE>;
	chomp $filename;
	close FILE;
	$filename = ( split( /\./, $filename ) )[0];
	unless ( $question->yesno( text => 'Do you want to keep '.$filename.' ?' ) ) {
		system( "rm ~/Downloads/.Video/".$filename.".*" );
	}
} elsif ($URL =~ $img_pattern) {
	my $type = (split(".",$URL))[-1];
	my $name = "web_image_".time.".".$type;
	system('cd ~/Downloads/.Pictures; curl "'.$URL.'" > '.$name.'; feh ~/Downloads/.Pictures/'.$name);
} 
#elsif ($URL =~ $archive) { 
#	my $name = "web_archive_".time."_".join("", map { s/(\/|\:)/_/g } split("/",$URL));
#	system('cd ~/Downloads; curl '.$URL.' > '.$name);
#}
elsif ($URL =~ $Twitch_pattern) {
	system("mpv $URL");
}

else {
	system('qutebrowser --target tab "'.$URL.'"');
}
