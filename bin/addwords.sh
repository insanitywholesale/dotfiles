#!/bin/sh

wcs=$(egrep "[0-9]+w$" "${1}"  | awk -F' ' '{print $NF}')
sum=0
for i in $wcs; do
        i=${i%?}
        sum=$((sum+i))
done

echo $sum
