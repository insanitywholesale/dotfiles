#!/bin/sh

choices="No\nYes\nmaybe"
answer=$(echo -e "$choices" | dmenu -i -p "Want to exit i3 ???" -nb '#222222' -sb '#e57c27' -l 3)
case "$answer" in
        Yes) killall -9 i3 ;;
        No) pgrep -x dunst && notify-send "not dead yet" ;;
        maybe) pgrep -x dunst && notify-send "tf u mean maybe?" ;;
esac
