#!/bin/bash
#btfo systemd

if [ $1 = start ] ; then
	rc-service $2 $1

elif [ $1 = stop ] ; then 
	rc-service $2 $1

elif [ $1 = restart ] ; then
	rc-service $2 $1

elif [ $1 = enable ] ; then
	rc-update add $2 default

elif [ $1 = disable ] ; then
	rc-update del $2 default

fi
