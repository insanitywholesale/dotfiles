#!/bin/sh -x

# Get id of touchpad and the id of the field corresponding to tapping to click
id=`xinput list | grep "Touchpad" | cut -d'=' -f2 | cut -d'[' -f1`
tap_to_click_id=`xinput list-props $id | grep "Tapping Enabled (" | cut -d'(' -f2 | cut -d')' -f1`

xinput --set-prop $id $tap_to_click_id 1
xinput --set-prop $id "Synaptics Tap Action" 1 1 1 1 1 1 1
xinput --set-prop $id "Synaptics Palm Detection" 1
synclient TapButton2=3
