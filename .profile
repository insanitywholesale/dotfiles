# ~/.profile

# executed by the command interpreter for login shells.

# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if in a tty set alacritty colors there too
if [ "$TERM" = "linux" ]; then
	echo -en "\e]P0181818" # normalblack
	echo -en "\e]P1AC4242" # normalred
	echo -en "\e]P290A959" # normalgreen
	echo -en "\e]P3F4BF75" # normalyellow
	echo -en "\e]P46A9FB5" # normalblue
	echo -en "\e]P5AA759F" # normalmagenta
	echo -en "\e]P675B5AA" # normalcyan
	echo -en "\e]P7D8D8D8" # normalwhite

	echo -en "\e]P86B6B6B" # brightgrey
	echo -en "\e]P9C55555" # brightred
	echo -en "\e]PAAAC474" # brightgreen
	echo -en "\e]PBFECA88" # brightyellow
	echo -en "\e]PC82B8C8" # brightblue
	echo -en "\e]PDC28CB8" # brightmagenta
	echo -en "\e]PE93D3C3" # brightcyan
	echo -en "\e]PFF8F8F8" # brightwhite

	clear                  # for background artifacting
fi

# if running bash
if [ -n "$BASH_VERSION" ]; then
	# include .bashrc if it exists
	if [ -f "$HOME/.bashrc" ]; then
		source "$HOME/.bashrc"
	fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
	PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
	PATH="$HOME/.local/bin:$PATH"
fi

# set PATH so it includes Go bin directory if it exists
if [ -d "$HOME/go/bin" ] ; then
	PATH="$HOME/go/bin:$PATH"
fi
