# ~/.bash_aliases

# general utility
alias setenv=". ~/.virtualenv/bin/activate"
alias clbin="curl -F 'clbin=<-' https://clbin.com/"
alias grubthingy="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias rustformat="find . -name Cargo.toml -exec cargo fmt --manifest-path '{}' ';'"
alias scs="maim -su | xclip -selection clipboard -t image/png"

# package management aliases
alias p="yay" # TODO: enable completion
alias clearshit='sudo pacman -Rns $(pacman -Qqdt)'
alias pkglistmain="diff -uw <(pacman -Qeq | sort) <(pacman -Qgq base base-devel | sort) | grep --color=never '^-' | tail -n +2 | sed -e 's/^-//'"

# enable color support of ls and also some handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	# ls aliases
	if type exa &> /dev/null; then
		alias ls="exa"
		alias l="exa -F"
		# check if it's eza and is version 10+ which includes octal perms
		if [ "$(exa -v | head -n 1 | cut -d' ' -f 1)" ] && [ "$(exa -v | head -n 2 | tail -n 1 | cut -d' ' -f 1 | cut -d '.' -f 2)" -gt 10 ]; then
			alias ll="exa -halo"
		else
			alias ll="exa -hal"
		fi
	else
		alias ls="ls --color=auto"
		alias l="ls -CF --color=auto"
		alias ll="ls -hal --color=auto"
	fi
	# non-ls stuff
	alias dir="dir --color=auto"
	alias vdir="vdir --color=auto"
	alias grep="grep --color=auto"
	alias fgrep="fgrep --color=auto"
	alias egrep="grep -E --color=auto"
	alias less="less -R"
else
	if type exa &> /dev/null; then
		alias ls="exa"
		alias l="exa -F"
		if [ "$(exa -v | head -n 1 | cut -d' ' -f 1)" ] && [ "$(exa -v | head -n 2 | tail -n 1 | cut -d' ' -f 1 | cut -d '.' -f 2)" -gt 10 ]; then
			alias ll="exa -halo"
		else
			alias ll="exa -hal"
		fi
	fi
	alias l="ls -CF"
	alias ll="ls -hal"
fi

# quick edit configs
EDITOR="vim"
alias xa="$EDITOR ~/.xinitrc"
alias ba="$EDITOR ~/.bashrc"
alias baa="$EDITOR ~/.bash_aliases"
alias va="$EDITOR ~/.vimrc"
alias na="nvim ~/.config/nvim/init.lua"
alias ta="$EDITOR ~/.config/tmux/tmux.conf"
alias aa="$EDITOR ~/.config/alacritty/alacritty.toml"

# TODO: enable completion
# command shorteners
alias v="vim"
alias vat="bat"
alias ccat="bat"
alias tmuxa="tmux a"
alias rgtd="rg -u TODO"
alias rgnt="rg -u NOTE"
alias tree="tree -L 3"
alias diff="diff --color"
alias diffdirs="diff --color -bur"
alias netman="NetworkManager"
alias cpuhigh="sudo cpupower frequency-set -g performance"
alias cpulow="sudo cpupower frequency-set -g powersave"
alias timefix="sudo ntpdate -s -b -u 0.gentoo.pool.ntp.org"
alias pswag="ps -Ho pid,stat,time,cmd --ppid 2 -p 2 --deselect"
alias printpath='printf "$PATH" | awk -F: '"'"'{OFS="\n"; $1=$1; print $0}'"'"''

# run test databases
alias runoldpostgres="docker run --rm -p 5432:5432 -e POSTGRES_PASSWORD=Apasswd -e POSTGRES_USER=$(whoami) postgres:9"
alias runnewpostgres="docker run --rm -p 5432:5432 -e POSTGRES_PASSWORD=Apasswd -e POSTGRES_USER=$(whoami) postgres:12"
alias testpostgres="docker run --name psql --rm -p 5432:5432 -e POSTGRES_PASSWORD=Apasswd -e POSTGRES_USER=tester postgres:latest postgres -c log_statement=all"
alias testoldmysql="docker run --rm --name=mysql -p 3306:3306 -e MYSQL_INITDB_SKIP_TZINFO=yes -e MYSQL_USER=tester -e MYSQL_PASSWORD=Apasswd -e MYSQL_DATABASE=tester -e MYSQL_ROOT_PASSWORD=rootApasswd mysql:5"
alias testmysql="docker run --rm --name=mysql -p 3306:3306 -e MYSQL_INITDB_SKIP_TZINFO=yes -e MYSQL_USER=tester -e MYSQL_PASSWORD=Apasswd -e MYSQL_DATABASE=tester -e MYSQL_ROOT_PASSWORD=rootApasswd mysql:latest"
alias testmariadb="docker run --rm --name=mariadb -p 3306:3306 -e MYSQL_INITDB_SKIP_TZINFO=yes -e MYSQL_USER=tester -e MYSQL_PASSWORD=Apasswd -e MYSQL_DATABASE=tester -e MYSQL_ROOT_PASSWORD=rootApasswd mariadb:latest"

# TODO: enable completion
# kubectl aliases
alias k="kubectl"
alias kgn="kubectl get nodes"
alias kgp="kubectl get pods"
alias kgpa="kubectl get pods -A"
alias kge="kubectl get events"
alias kgea="kubectl get events -A"

# fun aliases
alias minecraft="$HOME/bin/minecraft-launcher"
alias EXTERMINATE='killall -9'

# just for remembering stuff
alias newlinetospace="tr '\n' ' '"
alias findboi="echo 'find . -type f -exec sed -i 's/old/new/g' {} \;'"
alias jfxmodeneacss="vim ~/Downloads/jdk1.8.0_202/jre/lib/ext/com/sun/javafx/scene/control/skin/modena/modena.css"
